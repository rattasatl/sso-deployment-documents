# Docker Compose Deployment for SSO Project

This document outlines the Docker Compose configuration and deployment process for each models. Docker Compose is used to define and run multi-container Docker applications. This configuration assumes you have Docker and Docker Compose installed on your target system.

## How to build Docker image
Script ที่ใช้ build Docker image
```bash
docker buildx build -t sso-{service name}-backend:$(git describe --tags)-linux-amd64 --platform=linux/amd64 -f deployment/Dockerfile .
```
เปลี่ยน service name เป็นชื่อบริการต่างๆ ตามนี้
1. pps
2. vac
3. den
4. chronic
5. adjrw

### Example
ต้องการ build docker image ของค่าส่งเสริมสุขภาพและป้องกันโรค หรือ pps
```bash
docker buildx build -t sso-den-backend:$(git describe --tags)-linux-amd64 --platform=linux/amd64 -f deployment/Dockerfile .
```
หลังจาก build เสร็จแล้วจะได้ Docker image ตามนี้
![01-image-ls](img/01-image-ls.png)


## PPS
ระบบการตรวจสอบค่าบริการทางการแพทย์ด้วย AI
ค่าส่งเสริมสุขภาพและป้องกันโรค\
See [pps-deployment](pps-deployment.md)

## VAC
ระบบการตรวจสอบค่าบริการทางการแพทย์ด้วย AI
ค่าบริการส่งเสริมสุขภาพและป้องกันโรคกรณีวัคซีน\
See [vac-deployment](vac-deployment.md)

## DEN
ระบบการตรวจสอบค่าบริการทางการแพทย์ด้วย AI
ค่าบริการทางการแพทย์ กรณีทันตกรรม\
See [den-deployment](den-deployment.md)

## Chronic
ระบบการตรวจสอบค่าบริการทางการแพทย์ด้วย AI
ค่าบริการทางการแพทย์ตามภาระเสี่ยง\
See [chronic-deployment](chronic-deployment.md)

## ADJRW
ระบบการตรวจสอบค่าบริการทางการแพทย์ด้วย AI
ค่าบริการทางการแพทย์ประเภทผู้ป่วยในด้วยโรคที่มีค่าใช้จ่ายสูง (AdjRW >= 2)\
See [adjrw-deployment](adjrw-deployment.md)


# Data Input Paths

## ที่วางไฟล์ pps

### normal
    
    /data/sso-ai/sso-pps-deployment/mount_data_normal/pps_historical_input

### proactive

    /data/sso-ai/sso-pps-deployment/mount_data_proactive/pps_historical_input

## ที่วางไฟล์ vac

### normal

    /data/sso-ai/sso-vac-deployment/mount_data_normal/vac_historical_input

### proactive
    
    /data/sso-ai/sso-vac-deployment/mount_data_proactive/vac_historical_input

## ที่วางไฟล์ den

### normal

    /data/sso-ai/sso-den-deployment/mount_data_normal/den_historical_input

### proactive

    /data/sso-ai/sso-den-deployment/mount_data_proactive/den_historical_input

## ที่วางไฟล์ chronic

    /data/sso-ai/sso-chronic-deployment/mount_data/chronic_input

## ที่วางไฟล์ ADJRW

    /data/sso-ai/sso-adjrw-deployment/mount_data/adjrw_input
