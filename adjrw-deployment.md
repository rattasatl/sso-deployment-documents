# Docker Compose Deployment for SSO-ADJRW-Model

This document outlines the Docker Compose configuration and deployment process for SSO-ADJRW-Model. Docker Compose is used to define and run multi-container Docker applications. This configuration assumes you have Docker and Docker Compose installed on your target system.

## Prerequisites

1. Docker image file `sso-adjrw-backend-{version}-linux-amd64.tar`
2. file `.env` for docker-compose.yaml.
3. file `config.json` for model configurations.
4. file `server-config.json` for web server configurations.
5. file `docker-compose.yaml` for start the applications.

## Deployment Steps

### Prepare the environment file

1. แก้ไขไฟล์ `.env` โดยในแต่ละ key ในไฟล์จะกำหนดค่าดังนี้.
    - *IMAGE_TAG* \
    อยู่ในรูปแบบ `{version}-linux-amd64` \
    ตัวอย่างเช่น `1.2-linux-amd64`

    - *ADJRW_PORT* \
    เป็นเลข PORT ของ host ที่ map กับ container จะต้อง**ไม่ซ้ำ**กับ process หรือ container อื่นๆ โดยจะเป็นเลข PORT ของระบบการตรวจสอบค่าบริการทางการแพทย์ด้วย AI ค่าบริการทางการแพทย์ประเภทผู้ป่วยในด้วยโรคที่มีค่าใช้จ่ายสูง (AdjRW >= 2)

    - *HOST_INPUT_PATH* \
    เป็น path บน host ที่เก็บไฟล์ที่จะใช้สำหรับ retrain และ inference ของระบบการตรวจสอบค่าบริการทางการแพทย์ด้วย AI ค่าบริการทางการแพทย์ประเภทผู้ป่วยในด้วยโรคที่มีค่าใช้จ่ายสูง (AdjRW >= 2) โดย directory นั้นจะต้องมี sub-directory และไฟล์ที่จำเป็นดังนี้
        ```
        ├── input_data
        │    ├── adjrw_input
        │    │   ├── IPADT_yymm.csv
        │    │   ├── IPDXOP_yymm.csv
        │    ├── static
        │        ├── cluster.pkl
        │        ├── DRG_table.csv
        ...
        ```

    - *HOST_OUTPUT_PATH* \
    เป็น path บน host ที่เก็บไฟล์ที่จะได้จากการเรียกใช้ API inference ของระบบการตรวจสอบค่าบริการทางการแพทย์ด้วย AI ค่าบริการทางการแพทย์ประเภทผู้ป่วยในด้วยโรคที่มีค่าใช้จ่ายสูง (AdjRW >= 2)

    - *MODEL_CONFIG_PATH* \
    เป็น path บน host ที่ระบุไปยังไฟล์ config.json ที่ใช้เพื่อปรับแต่งค่าต่างๆ ของโมเดล

    - *SERVER_CONFIG_PATH* \
    เป็น path บน host ที่ระบุไปยังไฟล์ server-config.json ที่ใช้เพื่อปรับแต่งค่าต่างๆ ของ backend container ระบบการตรวจสอบค่าบริการทางการแพทย์ด้วย AI ค่าบริการทางการแพทย์ประเภทผู้ป่วยในด้วยโรคที่มีค่าใช้จ่ายสูง (AdjRW >= 2)

    ตัวอย่างไฟล์ .env
    ```env
    IMAGE_TAG=1-linux-amd64

    CHRONIC_NORMAL_PORT=8012

    HOST_INPUT_PATH=/host/path/to/normal_input_data
    
    HOST_OUTPUT_PATH=/host/path/to/output/normal
    
    MODEL_CONFIG_PATH=/host/path/to/config.json
    SERVER_CONFIG_PATH=/host/path/to/normal-server-config.json
    ```

2. แก้ไฟล์ `server-config.json` โดยแก้เฉพาะค่าดังนี้

    - `file_path_host` ให้เป็นค่าเดียวกันกับ `HOST_OUTPUT_PATH` ในไฟล์ `.env`
    - `ping_url` เป็น url ที่ต้องการให้แจ้งผลลัพธ์การประมวลผลกลับไปยัง server
    - `tablename_prefix` เป็นชื่อ prefix ของ table ใน database ให้ใส่เป็น `adjrw_`
    ```json
    {
        "file_path_host": "/host/path/to/output_data",
        "file_path_container": "data/adjrw_input",
        "ping_url": "http://HOSTNAME:PORT/ping",
        "database": {
            "hostname": "HOSTNAME",
            "port": PORT_NUMBER,
            "username": "username",
            "password": "password",
            "db": "database_name",
            "tablename_prefix": "adjrw_"
        },
        "dev": false
    }
    ```

3.  ไฟล์ `config.json` ไม่ต้องแก้ไข

## Load Docker image from compress file

1. Load Docker image from `sso-adjrw-backend-{version}-linux-amd64.tar`, where {version} is the version of the Docker image.
2. Run this command.
    ```sh
    docker load -i sso-adjrw-backend-{version}-linux-amd64.tar
    ``` 

## Start the applications

1. Start Docker compose with this command.
    ```sh
    docker compose up -d
    ```

## Stop the applications

1. Stop Docker compose with this command.
   ```sh
   docker compose down
   ```